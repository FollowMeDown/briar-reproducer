#!/usr/bin/env python3

import os
import subprocess
import sys

REPO_DIR = "briar"
REFERENCE_URL = 'https://briarproject.org/apk/briar-%s.apk'
GRADLE_TASK = "briar-android:assembleRelease"
APK_PATH = "briar-android/build/outputs/apk/release/briar-android-release-unsigned.apk"


def main():
    if len(sys.argv) > 2:
        fail("Usage: %s [tag]" % sys.argv[0])
    tag = sys.argv[1] if len(sys.argv) > 1 else None

    # clone/reset repo and checkout tag
    tag = prepare_repo(tag)

    # download reference binary (before building to detect missing file early on)
    version = tag.split('-')[1]
    url = REFERENCE_URL % version
    reference_apk = "briar-%s.apk" % version
    subprocess.check_call(['wget', '--no-verbose', url, '-O', reference_apk])

    # build the app
    repo_call(["./gradlew", GRADLE_TASK])

    # check if both APKs match
    apk = os.path.join(REPO_DIR, APK_PATH)
    if subprocess.call(['./verify-apk.py', reference_apk, apk]) == 0:
        print("Version '%s' was built reproducible! :)" % tag)


def prepare_repo(tag):
    if os.path.isdir(REPO_DIR):
        # get latest commits and tags from remote
        repo_call(['git', 'fetch', 'origin'])
        # checkout to latest master HEAD
        repo_call(['git', 'checkout', '-f', 'master'])
    else:
        # clone repo
        subprocess.check_call(['git', 'clone', os.environ.get("REPO_URL"), REPO_DIR])

    # undo all changes
    repo_call(['git', 'reset', '--hard'])

    # clean all untracked files and directories (-d) from repo
    repo_call(['git', 'clean', '-dffx'])

    # use latest tag if none given
    if tag is None:
        result = subprocess.check_output(['git', 'describe', '--abbrev=0', '--tags'], cwd=REPO_DIR)
        tag = result.decode().rstrip()  # strip away line-break

    # checkout tag
    repo_call(['git', 'checkout', tag])

    # return the tag that was used for the checkout
    return tag


def repo_call(command):
    subprocess.check_call(command, cwd=REPO_DIR)


def fail(msg=""):
    sys.stderr.write(msg + "\n")
    sys.exit(1)


if __name__ == "__main__":
        main()
