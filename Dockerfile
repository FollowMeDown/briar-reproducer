FROM debian:stretch

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
ENV ANDROID_HOME=/opt/android-sdk
ENV REPO_URL=https://code.briarproject.org/akwizgran/briar.git

WORKDIR /opt/briar-reproducer

# add required files to WORKDIR
ADD install*.sh ./
ADD verify-apk.py ./
ADD reproduce.py ./

RUN ./install.sh

CMD ./reproduce.py
