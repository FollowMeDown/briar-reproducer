#!/usr/bin/env python3

import hashlib
import os
import re
import subprocess
import sys
import tempfile
import zipfile

APK_SIG_FILE = re.compile(r'META-INF/[0-9A-Za-z_\-]+\.(SF|RSA|DSA|EC|MF)')


def main():
    if len(sys.argv) != 3:
        fail("Usage: %s referenceAPK builtAPK" % sys.argv[0])
    verify(sys.argv[1], sys.argv[2])


def verify(reference_apk, built_apk):
    if not os.path.isfile(reference_apk):
        fail("Can not verify: Reference APK '%s' does not exists" % reference_apk)

    if not os.path.isfile(built_apk):
        fail("Can not verify: Built APK '%s' does not exists" % built_apk)

    with tempfile.TemporaryDirectory() as tmp_dir:
        # repack reference APK
        reference_tmp = os.path.join(tmp_dir, os.path.basename(reference_apk))
        repack_apk(reference_apk, reference_tmp)

        # repack built APK
        built_tmp = os.path.join(tmp_dir, os.path.basename(built_apk))
        repack_apk(built_apk, built_tmp)

        # compare hashes of repacked APKs
        if calc_hash(reference_tmp) != calc_hash(built_tmp):
            sys.stderr.write("Mismatch detected. Trying to find reason...\n")
            subprocess.call(['diffoscope', reference_tmp, built_tmp])
            fail("Files do NOT match! :(")
        else:
            print("Files match! :)")
            sys.exit(0)


def repack_apk(zip_file, new_zip_file):
    # open original zip file
    with zipfile.ZipFile(zip_file, 'r') as f:
        # write to new zip file
        with zipfile.ZipFile(new_zip_file, 'w') as tmp:
            # go through all files
            for info in f.infolist():
                if APK_SIG_FILE.match(info.filename):
                    continue  # ignore signatures
                info.extra = b''  # reset file metadata
                # add file to new zip
                tmp.writestr(info, f.read(info.filename))


def calc_hash(filename, block_size=65536):
    sha512 = hashlib.sha512()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha512.update(block)
    return sha512.hexdigest()


def fail(msg=""):
    sys.stderr.write(msg + "\n")
    sys.exit(1)


if __name__ == "__main__":
        main()
